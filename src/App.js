import "./App.css";
import React, { Component } from "react";

import { connect } from "react-redux";

class App extends Component {
  render() {
    return (
      <div className="App">
        <p>hello world</p>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};
const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
