import * as actionTypes from "./actionTypes";

const intialState = {
  count: 0,
};

export default function reducer(state = intialState, action) {
  switch (action.type) {
    case actionTypes.INCREMENT:
      return {
        ...state,
        count: state.count + 1,
      };
    default:
      return state;
  }
}
