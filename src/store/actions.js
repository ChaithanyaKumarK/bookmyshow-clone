import * as actionTypes from "./actionTypes";

export const increment = (e) => ({
  type: actionTypes.INCREMENT,
  payload: {
    e,
  },
});
